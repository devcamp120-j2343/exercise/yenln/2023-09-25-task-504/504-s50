import { Car } from "./car.js";
class PetrolCar extends Car {
    fuelLevel;
    constructor(make, model, fuelLevel){
        super(make, model)
        this.fuelLevel = fuelLevel;
    }
    drive(){
        console.log(this.make);
        console.log(this.model)
        console.log(this.fuelLevel);
    }
    fillUp(){
        this.fuelLevel = "full";
    }
}

export {PetrolCar}