class Car {
    make; 
    model;
    constructor(make, model){
        this.make = make; 
        this.model = model; 
    }
    drive(){
        console.log(this.make);
        console.log(this.model)
    }
}

export {Car}