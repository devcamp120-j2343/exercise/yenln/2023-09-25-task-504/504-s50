 import { Car } from "./car.js";
class ElectricCar extends Car {
    batteryLevel;
    constructor(make, model, batteryLevel){
        super(make, model)
        this.batteryLevel = batteryLevel;
    }
    drive(){
        console.log(this.make);
        console.log(this.model)
        console.log(this.batteryLevel);
    }
    charge(){
        this.batteryLevel = 100;
    }
}

export {ElectricCar}