import { Car } from "./car.js";
import { ElectricCar } from "./electricCar.js";
import { PetrolCar } from "./PetrolCar.js";
var car1 = new Car("Toyota", "MX-11");
console.log(car1.drive());
console.log(car1 instanceof Car);

var electric1  = new ElectricCar("Vincome", "222-11", 30);
console.log(electric1.drive());
electric1.charge();
console.log(electric1.drive());


var petrol1  = new PetrolCar("Honda", "Queen1-122", "30");
console.log(petrol1.drive());
petrol1.fillUp();
console.log(petrol1.drive());
