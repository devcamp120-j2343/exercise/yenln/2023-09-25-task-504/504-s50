import { CommissionEmployee } from "./CommissionEmployee.js";
class BasePlusCommissionEmployee extends CommissionEmployee{
    baseSalary;
    constructor(firstName, lastName, socialSecurityNumber, grossSales, commissionRate,baseSalary){
        super(firstName, lastName, socialSecurityNumber, grossSales, commissionRate)
        this.baseSalary = baseSalary;
    }

}
export {BasePlusCommissionEmployee}