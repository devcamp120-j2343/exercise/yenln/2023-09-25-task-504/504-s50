import { Employee } from "./employee.js";
class CommissionEmployee extends Employee {
    grossSales ;
    commissionRate;
    constructor( firstName, lastName, socialSecurityNumber, grossSales, commissionRate){
        super(firstName, lastName, socialSecurityNumber)
        this.grossSales = grossSales; 
        this.commissionRate = commissionRate;
    }
}

export {CommissionEmployee}