import { Employee } from "./employee.js";
class HourlyEmployee extends Employee {
    wage ;
    hours;
    constructor( firstName, lastName, socialSecurityNumber, wage, hours){
        super(firstName, lastName, socialSecurityNumber)
        this.wage = wage; 
        this.hours = hours;
    }
}

export {HourlyEmployee}