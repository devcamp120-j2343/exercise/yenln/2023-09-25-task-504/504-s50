import { Employee } from "./employee.js";
class SalariedEmployee extends Employee {
    weeklySalary ;
    constructor(firstName, lastName, socialSecurityNumber, weeklySalary){
        super(firstName, lastName, socialSecurityNumber)
        this.weeklySalary = weeklySalary
    }
}

export {SalariedEmployee}