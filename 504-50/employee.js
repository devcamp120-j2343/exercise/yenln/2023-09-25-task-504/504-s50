class Employee {
    firstName;
    lastName;
    socialSecurityNumber;
    constructor(firstName, lastName, socialSecurityNumber){
        this.firstName = firstName;
        this.lastName = lastName;
        this.socialSecurityNumber = socialSecurityNumber;
    }

}

export {Employee}