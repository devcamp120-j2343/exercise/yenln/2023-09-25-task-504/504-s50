import { Employee } from "./employee.js";
import { CommissionEmployee } from "./CommissionEmployee.js";
import { HourlyEmployee } from "./HourlyEmployee.js";
import { SalariedEmployee } from "./SalariedEmployee.js";
import { BasePlusCommissionEmployee } from "./BasePlusCommissionEmployee.js";
var employee1 = new Employee("Hen", "Nhu", "SN-993333");
console.log(employee1.firstName);
console.log(employee1.lastName);
console.log(employee1.socialSecurityNumber);

var commission1 = new CommissionEmployee("Judia", "Horward", "8888-9Q",1000, 40);
console.log(commission1.grossSales);
console.log(commission1.commissionRate);


var hourly1 = new HourlyEmployee("Henry", "Horward", "849332-8x",10, 30);
console.log(hourly1.firstName);
console.log(hourly1.lastName);
console.log(hourly1.socialSecurityNumber);

console.log(hourly1.wage);
console.log(hourly1.hours);

var salary1 = new SalariedEmployee("Chu", "Tam", "3392918-10X", 500);
console.log(salary1.firstName);
console.log(salary1.lastName);
console.log(salary1.socialSecurityNumber);

console.log(salary1.weeklySalary);


var base1 = new BasePlusCommissionEmployee("Im", "IoE", "38392S-9Q",1000, 40,90);
console.log(base1.grossSales);
console.log(base1.commissionRate);
console.log(base1.baseSalary);
