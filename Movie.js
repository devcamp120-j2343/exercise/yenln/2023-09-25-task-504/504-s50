import { Product } from "./product.js";
class Movie extends Product{
    director;
    constructor( name, price, numberOfcopies, director){
        super(name, price, numberOfcopies);
        this.director = director;
    }
}

export {Movie}