import { Product } from "./product.js";
class Album extends Product{
    artist;
    constructor( name, price, numberOfcopies, artist){
        super(name, price, numberOfcopies);
        this.artist = artist;
    }
}

export {Album}