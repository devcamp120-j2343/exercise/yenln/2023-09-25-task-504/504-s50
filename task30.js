import { Product } from "./product.js";
import { Album } from "./album.js";
import { Movie } from "./Movie.js";
var product1 = new Product("Hakei", 100, 1000);
console.log(product1.sellCopies());
console.log( product1 instanceof Product)

var album1 = new Album("Jackie", 10, 900, "Hellen");
console.log(album1.name);


var movie1 = new Movie("Jackie", 10, 900, "Juun");
console.log(movie1.director);